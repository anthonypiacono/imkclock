$(document).ready(function() {
    $('body').delegate('input[type="submit"]', 'click', function() {
        if($(this).attr('action')) {
            $(this).parents('form').eq(0).attr('action', $(this).attr('action'));
        }

        return true;
    });

    $('body').delegate('a', 'click', function() {
        var async = $(this).attr('aj');

        if(async != 1) {
            return;
        }

        $.ajax({
            type: $(this).attr('method') || 'GET',
            url: $(this).attr('href'),
            complete: function(xhr) {
                $('body').append(xhr.responseText);
            }
        });

        return false;
    });

    $('body').delegate('form', 'submit', function() {
        var async = $(this).attr('aj');

        if(async != 1) {
            return;
        }

        var inputs = $(this).find(':input');

        var data = {};

        inputs.each(function() {
            data[this.name] = $(this).val();
        });

        $.ajax({
            type: $(this).attr('method') || 'post',
            url: $(this).attr('action') || document.location.pathname,
            data: data,
            complete: function(xhr) {
                $('body').append(xhr.responseText);
            }
        });

        return false;
    });
});