<?php
$config = array(
    new Route(array(
        'pattern' => '/',
        'resource' => 'HomeResource'
    )),
    new Route(array(
        'pattern' => '/login',
        'resource' => 'LoginResource'
    )),
    new Route(array(
        'pattern' => '/logout',
        'resource' => 'LogoutResource'
    )),
    new Route(array(
        'pattern' => '/begin-work',
        'resource' => 'BeginWorkResource'
    )),
    new Route(array(
        'pattern' => '/end-work',
        'resource' => 'EndWorkResource'
    )),
    new Route(array(
        'pattern' => '/create-invoice',
        'resource' => 'CreateInvoiceResource'
    )),
    new Route(array(
        'pattern' => '/edit-work/:work_id',
        'resource' => 'EditWorkResource'
    )),
    new Route(array(
        'pattern' => '/delete-work/:work_id',
        'resource' => 'DeleteWorkResource'
    )),
);