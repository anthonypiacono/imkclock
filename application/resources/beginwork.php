<?php

class BeginWorkResource extends ProtectedResource {
    public function execute() {
        if($this->request->get) {
            return new AppViewResponse($this, array(
                'view' => 'beginwork'
            ));
        }

        if($this->user->work_started !== null) {
            return $this->generic_dialog('Error', 'You must finish your current work first.');
        }

        $this->user->work_started = StrLib::DateTime();
        $this->user->work_description = $this->request->getData('description');

        $this->users->save($this->user);

        return $this->generic_dialog('OK', 'Work has begun.', null, <<<JS
$('.begin_work').hide();
$('.end_work').show();
JS
        );
    }
}