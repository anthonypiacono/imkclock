<?php

class LoginResource extends AppResource {
    public $next;

    public function __construct($properties = array()) {
        parent::__construct($properties);

        $this->next = isset($_GET['next']) ? $_GET['next'] : '/';
    }

    public function pre_execute() {
        if(null !== $this->user) {
            return $this->smart_redirect($this->next);
        }

        return true;
    }

    public function execute() {
        if ($this->request->post) {
            $username = $this->request->getData('username');

            $user = $this->users->firstBy('username', $username);

            if(null !== $user && $user->password == StrLib::Hash($this->request->getData('password'))) {
                $this->login($user->id);

                return $this->smart_redirect($this->next);
            }

            return $this->generic_error(array(
                'login' => array('Please check your username and password and try again')
            ));
        }

        return new AppViewResponse($this, array(
            'view' => 'login'
        ));
    }
}