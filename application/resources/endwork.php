<?php

class EndWorkResource extends ProtectedResource {
    public function execute() {
        if($this->request->get) {
            return new AppViewResponse($this, array(
                'view' => 'endwork'
            ));
        }

        if($this->user->work_started === null) {
            return $this->generic_dialog('Error', 'You haven\'t begun working yet.');
        }

        $description = $this->request->getData('description');

        if(empty($description)) {
            return $this->generic_dialog('Error', 'You must put in a description.', null, <<<JS
$.get('/end-work', function(t) {
    $('body').append(t);
});
JS
            );
        }

        $this->work->save(array(
            'user_id' => $this->user->id,
            'started' => $this->user->work_started,
            'ended' => StrLib::DateTime(),
            'description' => $description
        ));

        $this->user->work_description = null;
        $this->user->work_started = null;

        $this->users->save($this->user);

        return $this->generic_dialog('OK', 'Work is complete.', null, <<<JS
$('.end_work').hide();
$('.begin_work').show();
document.location.reload();
JS
        );
    }
}