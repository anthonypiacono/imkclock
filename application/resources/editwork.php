<?php

class EditWorkResource extends ProtectedResource {
    public function execute() {
        $work_id = $this->route->named['work_id'];

        $work = $this->work->firstBy('id', $work_id);

        if(null === $work || $work->user_id != $this->user->id) {
            return new Response(array(
                'status' => 404
            ));
        }

        if($this->request->post) {
            $start = strtotime($this->request->getData('start'));
            $end = strtotime($this->request->getData('end'));
            $description = $this->request->getData('description');

            if(empty($start) or empty($end) or empty($description)) {
                return $this->generic_dialog('Error', 'You must fill in a description, start date, and end date.', null, <<<JS
$.get('/edit-work/{$work_id}', function(t) {
   $('body').append(t);
});
JS
                );
            }

            $work->started = StrLib::DateTime($start);
            $work->ended = StrLib::DateTime($end);
            $work->description = $description;

            $this->work->save($work);

            return $this->generic_dialog('OK', 'Work saved.', null, <<<JS
document.location.reload();
JS
            );
        }

        return new AppViewResponse($this, array(
            'view' => 'editwork',
            'variables' => array(
                'work' => $work
            )
        ));
    }
}