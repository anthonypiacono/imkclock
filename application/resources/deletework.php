<?php

class DeleteWorkResource extends ProtectedResource {
    public function execute() {
        $work_id = $this->route->named['work_id'];

        $work = $this->work->firstBy('id', $work_id);

        if(null === $work || $work->user_id != $this->user->id) {
            return new Response(array(
                'status' => 404
            ));
        }

        if($this->request->post) {
            $this->database->query("DELETE FROM `work` WHERE id = '{$work_id}'");

            return $this->generic_dialog('OK', 'Work deleted.', null, <<<JS
document.location.reload();
JS
            );
        }

        return $this->generic_prompt('Are you sure?', 'Once you delete this, you can\'t recover it.', <<<JS
$.post('/delete-work/{$work_id}', function(t) {
    $('body').append(t);
});
JS
        , <<<JS
$.get('/edit-work/{$work_id}', function(t) {
    $('body').append(t);
});
JS
);
    }
}