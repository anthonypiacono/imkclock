<?php

Lib::Import('mysql/mysql_database');

Config::Import('mysql');

class AppResource extends Resource {
    /**
     * @var MySQLDatabase
     */
    public $database;

    /**
     * @var MySQLTable
     */
    public $users;

    /**
     * @var MySQLTable
     */
    public $work;

    public $user = null;

    public function __construct($properties = array()) {
        @session_start();

        parent::__construct($properties);

        $this->database = new MySQLDatabase(Config::$Configs['mysql']);

        $this->users = $this->database->table('users');

        $this->work = $this->database->table('work');

        $this->user = isset($_SESSION['user_id']) && $_SESSION['user_id'] !== false ?
            $this->users->firstBy('id', $_SESSION['user_id']) : null;
    }

    public function smart_redirect($url) {
        if($this->request->async) {
            $url = json_encode($url);

            return new Response(array(
                'body' => <<<HTML
<script type="text/javascript">
document.location = $url;
</script>
HTML
            ));
        }

        return new RedirectResponse(array(
            'location' => $url
        ));
    }

    public function login($user_id) {
        $_SESSION['user_id'] = $user_id;

        $this->user = $this->users->firstBy('id', $user_id);
    }

    public function logout() {
        $_SESSION['user_id'] = false;

        $this->user = null;
    }

    public function first_truth() {
        $arguments = func_get_args();

        foreach($arguments as $arg) {
            if(!empty($arg)) {
                return $arg;
            }
        }

        return null;
    }

    public function send_email($to, $subject, $message, $from = 'noreply@imkcreative.com') {
        $headers = "From: $from\r\n";
        $headers .= "Content-type: text/html\r\n";

        mail($to, $subject, $message, $headers);
    }

    public function message_box($title, $message, $buttons = array(), $after = null) {
        if(empty($buttons)) {
            $buttons = array('ok' => array(
                'text' => 'OK'
            ));
        }

        return new AppViewResponse($this, array(
            'view' => 'messagebox',
            'layout' => 'ajax',
            'variables' => array(
                'title' => $title,
                'message' => $message,
                'buttons' => $buttons,
                'after' => null === $after ? '' : $after
            )
        ));
    }

    public function generic_prompt($title, $message, $yes, $no = null, $cancel = false, $after = null) {
        $buttons = array();

        if($yes !== false) {
            $buttons['yes'] = array(
                'text' => 'Yes',
                'color' => 'Flatblue',
                'click' => null === $yes ? '' : $yes
            );
        }

        if($no !== false) {
            $buttons['no'] = array(
                'text' => 'No',
                'color' => 'Flatred',
                'click' => null === $no ? '' : $no
            );
        }

        if($cancel !== false) {
            $buttons['cancel'] = array(
                'text' => 'Cancel',
                'color' => 'FlatGrey',
                'click' => null === $cancel ? '' : $cancel
            );
        }

        return $this->message_box($title, $message, $buttons, $after);
    }

    public function generic_dialog($title = 'Notice', $message = 'There was an issue with your request.', $click = null, $after = null) {
        return $this->message_box($title, $message, array(
            'ok' => array(
                'text' => 'OK',
                'click' => null === $click ? '' : $click
            )
        ), $after);
    }

    public function first_key($a) {
        $keys = array_keys($a);

        return $keys[0];
    }

    public function first_error($errors) {
        return $errors[$this->first_key($errors)][0];
    }

    public function generic_error($errors, $after = null){
        $after = null === $after ? '' : $after;

        $first_key = $this->first_key($errors);

        return $this->generic_dialog('Error!', $this->first_error($errors), <<<JS
$('#{$first_key},:input[name="{$first_key}"]').eq(0).focus();

{$after}

JS
        );
    }

    public function parse_float_from_usd($usd) {
        $usd = str_replace('$', '', $usd);

        return (float)$usd;
    }

    public function time_ago($date_time) {
        $then = is_numeric($date_time) ? $date_time : strtotime($date_time);
        $now = time();

        $seconds_ago = $now - $then;

        if($seconds_ago < 60) {
            if ($seconds_ago == 1) {
                return $seconds_ago . ' second ago';
            }
            return $seconds_ago . ' seconds ago';
        }

        $minutes_ago = $seconds_ago / 60;

        if($minutes_ago < 60) {
            if ($minutes_ago == 1) {
                return round($minutes_ago) . ' minute ago';
            }
            return round($minutes_ago) . ' minutes ago';
        }

        $hours_ago = $minutes_ago / 60;

        if($hours_ago < 24) {
            if ($hours_ago == 1) {
                return round($hours_ago) . ' hour ago';
            }
            return round($hours_ago) . ' hours ago';
        }

        return date('F j, Y', $then);
    }
}