<?php

class CreateInvoiceResource extends ProtectedResource {
    public function execute() {
        if($this->request->get) {
            return new AppViewResponse($this, array(
                'view' => 'createinvoice'
            ));
        }

        $start = strtotime($this->request->getData('start'));
        $end = strtotime($this->request->getData('end'));

        if(empty($start) or empty($end)) {
            return $this->generic_dialog('Error', 'You must fill in a start and end date.', null, <<<JS
$.get('/create-invoice', function(t) {
   $('body').append(t);
});
JS
            );
        }

        $start = StrLib::DateTime($start);
        $end = StrLib::DateTime($end);

        $query = <<<SQL
SELECT * FROM `work`
WHERE (`started` >= '{$start}' AND `ended` <= '{$end}')
AND user_id = '{$this->user->id}'
SQL;

        $work = $this->database->selectQuery($query);

        $invoice = StrLib::Guid();

        $csv = fopen(Config::$Configs['application']['paths']['web_root'] . 'invoices/' . $invoice . ".csv", 'w');

        fputcsv($csv, array("Start", "Finish", "Description", "Total Hours"));

        foreach($work as $entry) {
            unset($entry->id);
            unset($entry->user_id);
            $entry = PhpSucks::ToIndexBasedArray($entry);
            $entry[] = (strtotime($entry[1]) - strtotime($entry[0])) / 3600;
            fputcsv2($csv, (array)$entry);
        }

        fclose($csv);

        return $this->message_box('OK', 'Download the invoice?', array(
            'download' => array(
                'text' => 'Download Invoice',
                'click' => <<<JS
document.location = '/invoices/{$invoice}.csv';
JS
            ),
            'cancel' => array(
                'text' => 'Cancel'
            )
        ));
    }
}