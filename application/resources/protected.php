<?php

Lib::Import('resources/authable');

class ProtectedResource extends AppResource {
    public function pre_execute() {
        if(null === $this->user) {
            return $this->smart_redirect('/login?next=' . urlencode($_SERVER['REQUEST_URI']));
        }

        return true;
    }
}