<?php

class LogoutResource extends AppResource {
    public function execute() {
        $this->logout();

        return $this->smart_redirect('/');
    }
}