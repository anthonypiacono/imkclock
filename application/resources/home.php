<?php

class HomeResource extends ProtectedResource {
    public function execute() {
        $work = $this->database->selectQuery("SELECT * FROM `work` WHERE user_id = '{$this->user->id}' ORDER BY id DESC");

        return new AppViewResponse($this, array(
            'view' => 'home',
            'variables' => array(
                'work' => $work
            )
        ));
    }
}