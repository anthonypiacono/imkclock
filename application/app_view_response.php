<?php

class AppViewResponse extends ViewResponse {
    /**
     * @var AppResource
     */
    public $resource;

    public function __construct(AppResource $resource, $properties = array()) {
        parent::__construct($properties);

        $this->resource = $resource;

        $this->layout = $this->resource->request->async ? 'ajax' : 'default';
    }
}