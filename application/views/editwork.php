<?php
ob_start();
?>
<form id="editwork" aj="1" method="post" action="<?= $this->resource->request->uri ?>">
    <fieldset>
        <p>Start Date</p>
        <?= $this->element('textbox', array(
            'name' => 'start',
            'default' => 'Start Date',
            'value' => $work->started
        )) ?>
        <br class="clear" />
        <?= $this->element('textbox', array(
            'name' => 'end',
            'default' => 'End Date',
            'value' => $work->ended
        )) ?>
        <br class="clear" />
        <?= $this->element('textarea', array(
            'name' => 'description',
            'default' => 'Description of Work',
            'value' => $work->description
        )) ?>
            <br class="clear" />
        <input type="submit" class="btnFlatBlue" value="Edit Work" />
        <input type="button" class="close btnFlatGrey" value="Cancel" />
        <a aj="1" href="/delete-work/<?= $work->id ?>">Delete</a>
    </fieldset>
</form>
<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?= $this->element('overlay', array(
    'title' => 'Edit Work',
    'body' => $body,
    'class' => 'edit_work'
)) ?>