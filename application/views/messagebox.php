<?php
ob_start();
?>
<p><?= $message ?></p>
<?php
foreach($buttons as $class => $button) {
    $buttons[$class]['id'] = uniqid();
    $color = isset($button['color']) ? $button['color'] : 'FlatGrey';
    $color[0] = strtoupper($color[0]);
?>
<button id="<?= $buttons[$class]['id'] ?>" class="btn<?= $color ?>">
    <?= htmlspecialchars($button['text']) ?>
</button>
<?php
}
$body = ob_get_contents();
ob_end_clean();

ob_start();
?>
<?php foreach($buttons as $button) { ?>
$('#<?= $button['id'] ?>').click(function() {

    <?= isset($button['click']) ? $button['click'] : '' ?>

    overlay.close();

    <?= isset($after) && null !== $after ? $after : '' ?>

});
<?php } ?>
<?php
$js = ob_get_contents();
ob_end_clean();
?>
<?= $this->element('overlay', array(
    'title' => $title,
    'body' => $body,
    'js' => $js,
    'click_mask_to_exit' => false,
    'class' => 'messagebox'
)) ?>