<?php
ob_start();
?>
<form id="endwork" aj="1" method="post" action="<?= $this->resource->request->uri ?>">
    <fieldset>
        <p>Description of Work</p>
        <?= $this->element('textarea', array(
            'name' => 'description',
            'default' => 'Description of Work',
            'value' => $this->resource->user->work_description
        )) ?>
        <br class="clear" />
        <input type="submit" class="btnFlatBlue" value="End Work" />
        <input type="button" class="close btnFlatGrey" value="Cancel" />
    </fieldset>
</form>
<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?= $this->element('overlay', array(
    'title' => 'End Work',
    'body' => $body,
    'class' => 'end_work'
)) ?>