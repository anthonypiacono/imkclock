<?php
$working = null !== $this->resource->user->work_started;
?>
<button <?= !$working ? 'style="display:none" ' : '' ?> class="end_work">End Work</button>
<button <?= $working ? 'style="display:none" ' : '' ?> class="begin_work">Begin Work</button>
<script>
    $('.end_work').click(function() {
        $.get('/end-work', function(t) {
            $('body').append(t);
        });

        return false;
    });

    $('.begin_work').click(function() {
        $.get('/begin-work', function(t) {
            $('body').append(t);
        });

        return false;
    });
</script>
