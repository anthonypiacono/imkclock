<div class="selectbox">
    <select style="display: none" name="<?= $name ?>">
        <option value=""></option>
        <?php foreach($items as $v => $text) { ?>
        <option value="<?= htmlspecialchars($v) ?>"><?= htmlspecialchars($text) ?></option>
        <?php } ?>
    </select>
    <div value="<?= isset($value) ? $value : '' ?>" class="selected_item"><?= isset($value) ? $items[$value] : $default ?></div>
    <div class="arrow"></div>
    <div class="flap">
        <div class="scroll-pane">
            <?php foreach($items as $value => $text) { ?>
            <div class="item" value="<?= htmlspecialchars($value) ?>"><?= htmlspecialchars($text) ?></div>
            <?php } ?>
        </div>
    </div>
</div>