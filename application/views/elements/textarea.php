<?php $default = isset($default) ? $default : ''; ?>
<div class="textarea <?= isset($class) ? $class : '' ?>">
    <textarea
        class="textarea <?= isset($class) ? $class : '' ?>"
        name="<?= isset($name) ? $name : '' ?>"
        default_text="<?= $default ?>"><?= isset($value) && $value !== null && !empty($value) ? $value : $default ?></textarea>
</div>