<?php $default = isset($default) ? $default : ''; ?>
<div class="textbox">
    <input type="text"
        class="textbox <?= isset($class) ? $class : '' ?>"
        name="<?= isset($name) ? $name : '' ?>"
        default_text="<?= $default ?>"
        value="<?= isset($value) && $value !== null && !empty($value) ? $value : $default ?>" />
</div>