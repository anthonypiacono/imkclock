<?php
$id = uniqid();
?>
<div class="<?= $class ?> overlay" style="display: none" id="<?= $id ?>">
<?php
if(isset($title)) {
?>
    <h2><?= $title ?></h2>
    <div>
        <div style="margin: 10px auto 10px auto; width: 186px" class="dottedLineImage"></div>
    </div>
<?php } ?>
    <?= $body ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.overlay[id!="<?= $id ?>"]').remove();

        var overlay = $('#<?= $id ?>').overlay({
            load: true,
            mask: {
                color: '#252525',
                opacity: 0.8,
                loadSpeed: 0,
                closeSpeed: 0
            },
            close: '',
            closeOnEsc: false,
            loadSpeed: 0,
            closeSpeed: 0,
            closeOnClick: <?= isset($click_mask_to_exit) && $click_mask_to_exit ? 'true' : 'false' ?>,
            top: 'center',
            api: true
        });

        $('#<?= $id ?> .close').click(function() {
            overlay.close();
            return false;
        });

    <?= isset($js) ? $js : '' ?>

    });
</script>
