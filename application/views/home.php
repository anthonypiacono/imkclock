Welcome <?= $this->resource->user->username ?><br />
<?= $this->element('work_button') ?>
<a href='/create-invoice' aj="1">Create Invoice</a>
    <style type="text/css">
        tr, th, td {
            text-align: left;
            border: 1px solid black;
            border-collapse: collapse;
            padding: 3px;
        }
        table {
            width: 100%;
            margin-top: 20px;
        }
    </style>
<table>
    <tr>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Description</th>
        <th>Total Hours</th>
        <th>Edit</th>
    </tr>
    <?php foreach($work as $entry) { ?>
    <tr>
        <td><?= htmlspecialchars($entry->started) ?></td>
        <td><?= htmlspecialchars($entry->ended) ?></td>
        <td><?= htmlspecialchars($entry->description) ?></td>
        <td><?= htmlspecialchars((strtotime($entry->ended) - strtotime($entry->started)) / 3600) ?></td>
        <td><a href="/edit-work/<?= $entry->id ?>" aj="1">Edit</a></td>
    </tr>
    <?php } ?>
</table>