<?php
ob_start();
?>
<form id="createinvoice" aj="1" method="post" action="<?= $this->resource->request->uri ?>">
    <fieldset>
        <p>Start Date</p>
        <?= $this->element('textbox', array(
            'name' => 'start',
            'default' => 'Start Date'
        )) ?>
        <br class="clear" />
        <?= $this->element('textbox', array(
            'name' => 'end',
            'default' => 'End Date'
        )) ?>
        <br class="clear" />
        <input type="submit" class="btnFlatBlue" value="Create Invoice" />
        <input type="button" class="close btnFlatGrey" value="Cancel" />
    </fieldset>
</form>
<?php
$body = ob_get_contents();
ob_end_clean();
?>
<?= $this->element('overlay', array(
    'title' => 'Create Invoice',
    'body' => $body,
    'class' => 'create_invoice'
)) ?>